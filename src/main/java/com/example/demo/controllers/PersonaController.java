package com.example.demo.controllers;
//Recibe la peticion web

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.PersonaModel;
import com.example.demo.services.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/persona")
public class PersonaController {
    
    @Autowired
    PersonaService personaService;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping()
    public ArrayList<PersonaModel> obtenerPersonas(){
        return personaService.obtenerPersonas();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping()
    public PersonaModel guardarUsuario(@RequestBody PersonaModel persona){
        return this.personaService.guardarPersona(persona);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/{id}")
    public Optional<PersonaModel> obtenerPorId(@PathVariable("id") int id){
        return this.personaService.obtenerPorId(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/query")
    public ArrayList<PersonaModel> obtenerPersonaPorApaterno(@RequestParam("apaterno") String apaterno){
        return this.personaService.obtenerPorApaterno(apaterno);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/{id}")
    public String eliminarPorId(@PathVariable("id") int id){
        boolean ok = this.personaService.eliminarPersona(id);
        if(ok){
            return "se elmimino la persona con id: "+id;
        } else{
            return null;
        }
    }
}
