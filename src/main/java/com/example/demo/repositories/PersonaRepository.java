package com.example.demo.repositories;
//conexiones con la base de datos

import java.util.ArrayList;

import com.example.demo.models.PersonaModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends CrudRepository<PersonaModel, Integer>{
    public abstract ArrayList<PersonaModel> findByApaterno(String apaterno);
}
