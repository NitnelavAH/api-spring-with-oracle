package com.example.demo.services;
//ejecuta la logica de la app

import java.util.ArrayList;
import java.util.Optional;

import com.example.demo.models.PersonaModel;
import com.example.demo.repositories.PersonaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaService {
    @Autowired
    PersonaRepository personaRepository;

    public ArrayList<PersonaModel> obtenerPersonas(){
       return (ArrayList<PersonaModel>) personaRepository.findAll();
    }

    public PersonaModel guardarPersona(PersonaModel persona){
        return personaRepository.save(persona);
    }

    public Optional<PersonaModel> obtenerPorId(int id){
        return personaRepository.findById(id);
    }

    public ArrayList<PersonaModel> obtenerPorApaterno(String apaterno){
        return personaRepository.findByApaterno(apaterno);
    }

    public boolean eliminarPersona(int id){
        try {
            personaRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
