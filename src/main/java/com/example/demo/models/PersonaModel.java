/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models;
//tipo de informacion
import javax.persistence.*;

/**
 *
 * @author ValentinAbundo
 */
@Entity
@Table(name="personas")
public class PersonaModel {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PER_SEQ")
    @SequenceGenerator(sequenceName = "persona_id", allocationSize = 1, name = "PER_SEQ")
    private int id;
    
    private String nombre;
    private String apaterno;
    private String amaterno;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApaterno() {
        return apaterno;
    }

    public void setApaterno(String apaterno) {
        this.apaterno = apaterno;
    }

    public String getAmaterno() {
        return amaterno;
    }

    public void setAmaterno(String amaterno) {
        this.amaterno = amaterno;
    }
    
}
